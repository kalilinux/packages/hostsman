From: Steev Klimaszewski <steev@kali.org>
Date: Mon, 5 Feb 2024 13:40:53 -0600
Subject: Use assertEqual not assertEquals

* assertEquals was removed in Python 3.12; PR #11
* Fix Syntax Warnings (use raw strings); PR #10

Forwarded: https://github.com/qszhuan/hostsman/pull/11
Applied-Upstream: commit:3ea51f4, https://github.com/qszhuan/hostsman/pull/11
---
 tests/host_test.py | 48 ++++++++++++++++++++++++------------------------
 1 file changed, 24 insertions(+), 24 deletions(-)

diff --git a/tests/host_test.py b/tests/host_test.py
index 175af68..645b161 100644
--- a/tests/host_test.py
+++ b/tests/host_test.py
@@ -36,24 +36,24 @@ class TestHost(unittest.TestCase):
     @patch('sys.platform', 'linux')
     def test_get_correct_host_file_for_linux(self):
         host = Host()
-        self.assertEquals('/etc/hosts', host.hostFile)
+        self.assertEqual('/etc/hosts', host.hostFile)
 
     @patch('sys.platform', 'win32')
     def test_get_correct_host_file_for_windows(self):
         host = Host()
-        self.assertEquals('c:\windows\system32\drivers\etc\hosts', host.hostFile)
+        self.assertEqual(r'c:\windows\system32\drivers\etc\hosts', host.hostFile)
 
     @patch('sys.platform', 'darwin')
     def test_get_correct_host_file_for_osx(self):
         host = Host()
-        self.assertEquals('/etc/hosts', host.hostFile)
+        self.assertEqual('/etc/hosts', host.hostFile)
 
     def test_get_list(self):
         host_list = self.host.list()
-        self.assertEquals(len(host_list), 3)
-        self.assertEquals("127.0.0.1\tmy.test1", host_list[0])
-        self.assertEquals("127.0.0.2\tmy.test2 my.test3 my.test1", host_list[1])
-        self.assertEquals("127.0.0.3\tmy.test4", host_list[2])
+        self.assertEqual(len(host_list), 3)
+        self.assertEqual("127.0.0.1\tmy.test1", host_list[0])
+        self.assertEqual("127.0.0.2\tmy.test2 my.test3 my.test1", host_list[1])
+        self.assertEqual("127.0.0.3\tmy.test4", host_list[2])
 
     def test_if_host_existed(self):
         existed = self.host.exists('my.test1')
@@ -71,50 +71,50 @@ class TestHost(unittest.TestCase):
 
     def test_check_host(self):
         result = self.host.check('my.test1')
-        self.assertEquals(2, len(result))
-        self.assertEquals('127.0.0.1\tmy.test1', result[0])
-        self.assertEquals('127.0.0.2\tmy.test2 my.test3 my.test1', result[1])
+        self.assertEqual(2, len(result))
+        self.assertEqual('127.0.0.1\tmy.test1', result[0])
+        self.assertEqual('127.0.0.2\tmy.test2 my.test3 my.test1', result[1])
 
     def test_check_non_host(self):
         result = self.host.check('aeiou')
-        self.assertEquals(0, len(result))
+        self.assertEqual(0, len(result))
         # self.assertIsNone(result)
 
     def test_check_multi_hosts(self):
         result = self.host.check('my.test1', 'my.test2')
-        self.assertEquals(2, len(result))
-        self.assertEquals('127.0.0.1\tmy.test1', result[0])
-        self.assertEquals('127.0.0.2\tmy.test2 my.test3 my.test1', result[1])
+        self.assertEqual(2, len(result))
+        self.assertEqual('127.0.0.1\tmy.test1', result[0])
+        self.assertEqual('127.0.0.2\tmy.test2 my.test3 my.test1', result[1])
     
     def test_add_host(self):
         success = self.host.add("add.test.com", "127.0.0.4")
         self.assertTrue(success)
-        self.assertEquals(4, len(self.host.list()))
+        self.assertEqual(4, len(self.host.list()))
         
         result = self.host.check("add.test.com")
-        self.assertEquals("127.0.0.4\tadd.test.com", result[0])
+        self.assertEqual("127.0.0.4\tadd.test.com", result[0])
     
     def test_add_host_to_existing_ip(self):
         success = self.host.add("add.test.com")
         self.assertTrue(success)
         
-        self.assertEquals(3, len(self.host.list()))
+        self.assertEqual(3, len(self.host.list()))
         result = self.host.check("add.test.com")
-        self.assertEquals("127.0.0.1\tmy.test1 add.test.com", result[0])
+        self.assertEqual("127.0.0.1\tmy.test1 add.test.com", result[0])
 
     def test_remove_non_existing_host(self):
         hostname = "add.test.com"
         success,_ = self.host.remove(hostname)
         self.assertFalse(success)
         self.assertFalse(self.host.exists(hostname))
-        self.assertEquals(3, len(self.host.list()))
+        self.assertEqual(3, len(self.host.list()))
     
     def test_remove_host(self):
         hostname = "my.test1"
         success,_ = self.host.remove(hostname)
         self.assertTrue(success)
         self.assertFalse(self.host.exists(hostname))
-        self.assertEquals(2, len(self.host.list()))
+        self.assertEqual(2, len(self.host.list()))
 
     @patch('sys.platform', 'win32')
     def test_should_split_to_separate_line_if_hostname_alias_more_than_9_in_windows(self):
@@ -125,11 +125,11 @@ class TestHost(unittest.TestCase):
         for i in range(1, 10):
             success = self.host.add(hostname + "." + str(i), '127.10.10.10')
             self.assertTrue(success)
-        self.assertEquals(4, len(host.list()))
+        self.assertEqual(4, len(host.list()))
 
         success = self.host.add(hostname + ".10", '127.10.10.10')
         self.assertTrue(success)
-        self.assertEquals(5, len(host.list()))
+        self.assertEqual(5, len(host.list()))
 
         for i in range(1, 11):
             self.assertTrue(self.host.exists(hostname + "." + str(i)))
@@ -143,11 +143,11 @@ class TestHost(unittest.TestCase):
         for i in range(1, 10):
             success = self.host.add(hostname + "." + str(i), '127.10.10.10')
             self.assertTrue(success)
-        self.assertEquals(4, len(host.list()))
+        self.assertEqual(4, len(host.list()))
 
         success = self.host.add(hostname + ".10", '127.10.10.10')
         self.assertTrue(success)
-        self.assertEquals(4, len(host.list()))
+        self.assertEqual(4, len(host.list()))
 
         for i in range(1, 11):
             self.assertTrue(self.host.exists(hostname + "." + str(i)))
